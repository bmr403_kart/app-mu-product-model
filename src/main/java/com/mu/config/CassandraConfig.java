package com.mu.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.core.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.core.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@Configuration
@PropertySource(value = {"classpath:application.properties"}, ignoreResourceNotFound = false)
@EnableCassandraRepositories(
  basePackages = "com.mu.config")
public class CassandraConfig extends AbstractCassandraConfiguration {
	
	private static final Logger logger = LoggerFactory.getLogger(CassandraConfig.class);

	 @Value("${spring.data.cassandra.contact-points}")
	 private String contactPoints;

	 @Value("${spring.data.cassandra.keyspace-name}")
	 private String keyspaceName;

	 @Value("${spring.data.cassandra.port}")
	 private Integer port;
	
	
	@Override
    protected String getKeyspaceName() {
        return keyspaceName;
    }
 
    @Bean
    public CassandraClusterFactoryBean cluster() {
        CassandraClusterFactoryBean cluster = 
          new CassandraClusterFactoryBean();
        cluster.setContactPoints(contactPoints);
        cluster.setPort(port);
        return cluster;
    }
 
    @Bean
    public CassandraMappingContext cassandraMapping() 
      throws ClassNotFoundException {
        return new BasicCassandraMappingContext();
    }
}
