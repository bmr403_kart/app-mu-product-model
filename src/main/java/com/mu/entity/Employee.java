/*
 * 
 */
package com.mu.entity;

import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import com.datastax.driver.core.DataType.Name;

/**
 * @author LENOVO
 *
 */

@Table("emp")
public class Employee {
	
	@PrimaryKey
	@CassandraType(type = Name.INT)
	private String empid;
	
	@Column
	private String emp_first;
	
	@Column
	private String emp_last;
	
	@Column
	private String emp_dept;

	public String getEmpid() {
		return empid;
	}

	public void setEmpid(String empid) {
		this.empid = empid;
	}

	public String getEmp_first() {
		return emp_first;
	}

	public void setEmp_first(String emp_first) {
		this.emp_first = emp_first;
	}

	public String getEmp_last() {
		return emp_last;
	}

	public void setEmp_last(String emp_last) {
		this.emp_last = emp_last;
	}

	public String getEmp_dept() {
		return emp_dept;
	}

	public void setEmp_dept(String emp_dept) {
		this.emp_dept = emp_dept;
	}
	
	
}
